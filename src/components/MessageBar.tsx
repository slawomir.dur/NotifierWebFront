import * as React from 'react'
import { Paper, Typography } from '@mui/material'

interface MessageBarProps {
    info: string
}

const MessageBar: React.FC<MessageBarProps> = (props: MessageBarProps) => {
    return (
        <Paper
            elevation={3}
            sx={{
                marginTop: 8,
                display: "flex",
                flexDirection: "column",
                justifyContent: 'flex-end',
                alignItems: "center",
                p: 5
            }}
            >
            <Typography component="h1" variant="h5" sx={{ m: 1 }}>
                {props.info}
            </Typography>
        </Paper>
    )
}

export default MessageBar;