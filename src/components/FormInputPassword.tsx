import { Controller, useFormContext } from 'react-hook-form'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import OutlinedInput from '@mui/material/OutlinedInput'
import FormHelperText from '@mui/material/FormHelperText'
import { IconButton, TextField } from '@mui/material'
import React from 'react'
import Visibility from '@mui/icons-material/Visibility'
import VisibilityOff from '@mui/icons-material/VisibilityOff'

type FormInputPasswordProps = {
    name: string
    label: string,
    rules?: any
}

const FormInputPassword: React.FC<FormInputPasswordProps> = ({ name, label, rules, ...otherProps }) => {
    const [showPassword, setShowPassword] = React.useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);
    const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    const {
        control,
        formState: { errors }
    } = useFormContext();

    return (
        <Controller
            name={name}
            control={control}
            rules={{
                ...rules
            }}
            render={({
                field
            }) => (
                <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                    <TextField
                        {...field}
                        InputLabelProps={{shrink: true}}
                        id={name}
                        label={label}
                        type="password"
                        size="small"
                        sx = {{mt: 2}}
                        error={!!errors[name]}
                        helperText={!!errors[name] ? errors[name]?.message as string : null}
                    />
                </FormControl>
            )} />
    )
}

export default FormInputPassword;
