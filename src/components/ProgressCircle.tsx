import * as React from 'react'
import { Paper, Typography, CircularProgress } from '@mui/material'


const ProgressCircle: React.FC = () => {
    return (
        <Paper
            elevation={3}
            sx={{
                marginTop: 8,
                display: "flex",
                flexDirection: "column",
                justifyContent: 'flex-end',
                alignItems: "center",
                p: 5
            }}
        >
            <Typography component="h1" variant="h5" sx={{ m: 1 }}>
                Ładowanie danych...
            </Typography>
            <CircularProgress />
        </Paper>
    )
}

export default ProgressCircle;