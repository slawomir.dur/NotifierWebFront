import * as React from 'react'
import LoginForm from './login-form/LoginForm'
import Container from '@mui/material/Container'
import { Box, Typography } from '@mui/material'

const LoginPage: React.FC = () => {
    return (
        <Container
            component="main"
            maxWidth="xs">
                <Box
                    sx={{
                        marginTop: 8,
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center"
                    }}
                >
                    <Typography component="h1" variant="h5">
                        Zaloguj się
                    </Typography>
                    <LoginForm />
                </Box>
            
        </Container>
    )
}

export default LoginPage;