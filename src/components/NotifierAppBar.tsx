import * as React from 'react'
import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import IconButton from '@mui/material/IconButton'
import { useAuth } from '../auth/AuthProvider'

const NotifierAppBar: React.FC = () => {
    const { logout } = useAuth()

    return (
        <AppBar position="fixed" sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}>
            <Toolbar>
                <IconButton
                    size="large"
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    sx={{ mr: 2 }}
                >
                </IconButton>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    Notificator
                </Typography>
                <Button onClick={logout} color="inherit">Wyloguj się</Button>
            </Toolbar>
        </AppBar>
    )
}

export default NotifierAppBar;