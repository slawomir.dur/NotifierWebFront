import * as React from 'react'
import { Box, Typography } from '@mui/material'

const styles = {
    container: {
        border: '1px dashed grey', // Border style
        borderRadius: '4px',
        padding: '8px', // Add padding for spacing
        position: 'relative', // Relative positioning for the label,
        m: 1
      },
      label: {
        color: 'grey',
        position: 'absolute', // Absolute positioning to place label on the border
        top: '-10px', // Adjust the vertical positioning as needed
        left: '8px', // Adjust the horizontal positioning as needed
        backgroundColor: '#fff', // Background color for the label
        padding: '0 4px', // Add padding to the label for spacing
        fontSize: '12px', // Adjust font size as needed
      },
}

const OutlinedTitledBox: React.FC<any> = ({ children, sx, title }) => {
    return (
        <Box sx={{...styles.container, ...sx}}>
          <Typography variant="body2" sx={styles.label}>
            {title}
          </Typography>
          {children}
        </Box>
      );
}

export default OutlinedTitledBox;