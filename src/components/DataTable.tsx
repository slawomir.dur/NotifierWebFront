import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Typography } from '@mui/material';

export default function DataTable({ dataContract, data }: any) {

    if (data.length === 0) {
        return (
            <Typography>Brak danych do wyświetlenia</Typography>
        )
    }
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        {
                            Object.entries(dataContract).map(
                                ([key, value], index) => {
                                    if (index === 0) {
                                        return (
                                            <TableCell>{value}</TableCell>
                                        )
                                    }
                                    return (
                                        <TableCell align="right">{value}</TableCell>
                                    )
                                }
                            )
                        }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map((user) => (
                        <TableRow
                            key={user.uuid}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            {Object.entries(dataContract).map(([key], index) => (
                                <TableCell align={index === 0 ? "left" : "right"}>{user[key]}</TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
