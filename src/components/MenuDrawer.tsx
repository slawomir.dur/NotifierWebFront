import * as React from 'react'
import {
    Drawer, List,
    ListItem, ListItemIcon, ListItemButton,
    ListItemText,
    Toolbar
} from '@mui/material'
import Box from '@mui/material/Box'
import { modelMappings } from '../model/model-mapping'

const MenuDrawer: React.FC = () => {
    return (
        <>
            <Drawer
                variant="permanent"
                sx={{
                    width: 240,
                    flexShrink: 0,
                    [`& .MuiDrawer-paper`]: { width: 240, boxSizing: 'border-box' }
                }}>
                <Toolbar />
                <Box sx={{ overflow: 'auto' }}>
                    <List>
                        {
                            modelMappings.map(
                                (model) => {
                                    return (
                                        <ListItem key={model.name} disablePadding>
                                                <ListItemButton href={model.route}>
                                                    <ListItemIcon>
                                                        {model.icon}
                                                    </ListItemIcon>
                                                    <ListItemText primary={model.label} />
                                                </ListItemButton>
                                        </ListItem>
                                    )
                                }
                            )
                        }
                    </List>
                </Box>
            </Drawer>
        </>
    )
}

export default MenuDrawer;