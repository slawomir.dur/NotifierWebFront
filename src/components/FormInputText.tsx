import { Controller, useFormContext } from 'react-hook-form'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import OutlinedInput from '@mui/material/OutlinedInput'
import OutlinedInputProps from '@mui/material/OutlinedInput'
import FormHelperText from '@mui/material/FormHelperText'
import { TextField } from '@mui/material'

type FormInputProps = {
    name: string
    label: string,
    rules?: any
}

const FormInputText: React.FC<FormInputProps> = ({ name, label, rules, ...otherProps }) => {
    const {
        control,
        formState: { errors }
    } = useFormContext();

    return (
        <Controller
            name={name}
            control={control}
            rules={{
                ...rules
            }}
            render={({
                field
            }) => (
            <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                <TextField
                    {...field}
                    InputLabelProps={{shrink: true}}
                    id={name}
                    label={label}
                    size="small"
                    sx = {{mt: 2}}
                    error={!!errors[name]}
                    helperText={!!errors[name] ? errors[name]?.message as string : null}
                />
            </FormControl>
            )} />
    )
}

export default FormInputText;
