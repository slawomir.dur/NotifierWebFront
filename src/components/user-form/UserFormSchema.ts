import { object, string, number, TypeOf } from 'zod'

export const newUserSchema = object({
    username: string()
        .min(1, 'Nazwa użytkownika jest wymagana')
        .max(32, 'Nazwa użytkownika jest za długa'),
    password: string()
        .min(1, 'Hasło jest wymagane')
        .max(32, 'Hasło jest za długie'),
    repeat_password: string(),
    role: number(),
    first_name: string()
        .max(32, 'Imię jest za długie'),
    last_name: string()
        .max(32, 'Nazwisko jest za długie'),
    email: string()
        .max(32, 'Email jest za długi')

});

export const editUserSchema = object({
    username: string()
        .min(1, 'Nazwa użytkownika jest wymagana')
        .max(32, 'Nazwa użytkownika jest za długa'),
    role: number(),
    first_name: string()
        .max(32, 'Imię jest za długie'),
    last_name: string()
        .max(32, 'Nazwisko jest za długie'),
    email: string()
        .max(32, 'Email jest za długi')

});

export type NewUserInput = TypeOf<typeof newUserSchema>;
export type EditUserInput = TypeOf<typeof editUserSchema>;