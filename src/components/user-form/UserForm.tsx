import * as React from 'react'
import { Box, Button, Paper, Typography, Card, Container } from '@mui/material'
import { useForm, SubmitHandler, FormProvider } from 'react-hook-form'
import FormInputText from '../FormInputText'
import FormInputPassword from '../FormInputPassword'
import { zodResolver } from '@hookform/resolvers/zod'
import { newUserSchema, editUserSchema, NewUserInput, EditUserInput } from './UserFormSchema'
import FormInputSelect from '../FormInputSelect'
import OutlinedTitledBox from '../OutlinedTitledBox'

interface UserFormProps {
    action: (user: any) => void,
    buttonText: string,
    title: string,
    user: object | null
}

const UserForm: React.FC<UserFormProps> = (props: UserFormProps) => {
    console.log(props)
    const userSchema = props.user === null ? newUserSchema : editUserSchema;
    const defaultValues = props.user === null ? {
        username: "",
        password: "",
        repeat_password: "",
        role: 3,
        email: "",
        first_name: "",
        last_name: ""
    } : {
        username: props.user.username,
        role: props.user.role,
        email: props.user.email,
        first_name: props.user.first_name,
        last_name: props.user.last_name
    }

    const methods = useForm<NewUserInput | EditUserInput>({
        resolver: zodResolver(userSchema),
        defaultValues: defaultValues
    })

    const onSubmit: SubmitHandler<NewUserInput | EditUserInput> = (data) => {
        if (props.user === null) {
            props.action(data);
        } else {
            props.action({ ...data, uuid: props.user.uuid })
        }
    }

    return (
        <Box
            sx={{
                p: 1,
                display: "flex",
                flexDirection: "column",
                alignItems: "center"
            }}
        >
            <Typography component="h1" variant="h5" sx={{ m: 2 }}>{props.title}</Typography>
            <FormProvider
                {...methods}
            >
                <Box
                    component="form"
                    noValidate
                    autoComplete='off'
                    onSubmit={methods.handleSubmit(onSubmit)}
                    // justifyContent='center'
                    // alignItems='center'>
                    sx={{
                        display: "flex",
                        flexDirection: "column",
                    }}
                >
                    <OutlinedTitledBox title="Konto" sx={{
                        display: "flex",
                        flexDirection: "column"
                    }}
                    >
                        <Box sx={{
                            display: "flex",
                            flexDirection: "row",
                        }}>
                            <FormInputText
                                name="username"
                                label="Nazwa użytkownika"
                            />
                            <FormInputSelect
                                name="role"
                                label="Rola"
                                menuItems={[{ value: 1, key: "Administrator" },
                                { value: 2, key: "Edytor" },
                                { value: 3, key: "Użytkownik" }]}
                            />
                        </Box>
                        <Box sx={{
                            display: "flex",
                            flexDirection: "row",
                        }} >

                            {
                                props.user === null && (
                                    <>
                                        <FormInputPassword
                                            name="password"
                                            label="Hasło" />
                                        <FormInputPassword
                                            name="repeat_password"
                                            label="Powtórz hasło" />
                                    </>
                                )
                            }

                        </Box>
                    </OutlinedTitledBox>

                    <OutlinedTitledBox title="Dane personalne" sx={{
                        display: "flex",
                        flexDirection: "row",
                    }} >
                        <FormInputText
                            name="first_name"
                            label="Imię"
                        />
                        <FormInputText
                            name="last_name"
                            label="Nazwisko"
                        />

                    </OutlinedTitledBox>
                    <OutlinedTitledBox title="Dane kontaktowe" sx={{
                        display: "flex",
                        flexDirection: "row",
                    }} >
                        <FormInputText
                            name="email"
                            label="Email"
                        />
                        <FormInputText
                            name="phone"
                            label="Telefon"
                        />

                    </OutlinedTitledBox>
                    <Button
                        type="submit"
                        sx={{ m: 1, marginTop: 5 }}
                        variant="contained">
                        {props.buttonText}
                    </Button>
                </Box>
            </FormProvider>
        </Box>
    )
}

export default UserForm;