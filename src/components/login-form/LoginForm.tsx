import * as React from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import { useForm, SubmitHandler, FormProvider } from 'react-hook-form'
import FormInputText from '../FormInputText'
import FormInputPassword from '../FormInputPassword'
import { postAuth } from '../../api/httpAuth'
import { AuthResponse } from '../../api/interfaces/authInterfaces'
import { useNavigate } from 'react-router-dom'
import { routing, fromRoot } from '../../routes/routes'
import { zodResolver } from '@hookform/resolvers/zod'
import { loginSchema, LoginInput } from './LoginFormSchema'
import { useAuth } from '../../auth/AuthProvider'
import { Navigate } from 'react-router-dom'


const LoginForm: React.FC = () => {
    const methods = useForm<LoginInput>({
        resolver: zodResolver(loginSchema),
        defaultValues: {
            username: "",
            password: ""
        }
    });

    const { token, login } = useAuth();

    const navigate = useNavigate();

    const onSubmit: SubmitHandler<LoginInput> = (data) => {
        postAuth(data).then(
            (response) => {
                const auth: AuthResponse = response.data;
                console.log(auth);
                login(auth);
                navigate(fromRoot(routing.users));
            }
        ).catch((reason) => alert(reason))
    };

    if (token !== "") {
        return (
            <Navigate to={fromRoot(routing.users)} />
        )
    }

    return (
        <FormProvider
            {...methods}
        >
            <Box
                component="form"
                noValidate
                autoComplete='off'
                onSubmit={methods.handleSubmit(onSubmit)}
                justifyContent="center"
                alignItems="center">
                <Box>
                    <FormInputText
                        name="username"
                        label="Nazwa użytkownika" />
                </Box>
                <Box>
                    <FormInputPassword
                        name="password"
                        label="Hasło" />
                </Box>
                <Box>
                    <Button
                        fullWidth
                        type="submit"
                        sx={{ m: 1 }} 
                        variant="contained">
                        Zaloguj się
                    </Button>
                </Box>
            </Box>
        </FormProvider>
    )
}

export default LoginForm;