import { object, string, TypeOf } from 'zod'

export const loginSchema = object({
    username: string()
        .min(1, 'Nazwa użytkownika jest wymagana')
        .max(32, 'Nazwa użytkownika jest za długa'),
    password: string()
        .min(1, 'Hasło jest wymagane')
        .max(32, 'Hasło jest za długie')
});

export type LoginInput = TypeOf<typeof loginSchema>;