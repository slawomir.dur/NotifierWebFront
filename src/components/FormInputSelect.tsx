import { Controller, useFormContext } from 'react-hook-form'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import FormHelperText from '@mui/material/FormHelperText'
import { TextField, MenuItem } from '@mui/material'

interface FormInputSelectMenuItem {
    value: any,
    key: string
}

type FormInputSelectProps = {
    name: string
    label: string,
    rules?: any,
    menuItems: FormInputSelectMenuItem[]
}

const FormInputSelect: React.FC<FormInputSelectProps> = ({ name, label, rules, menuItems, ...otherProps }) => {
    const {
        control,
        formState: { errors }
    } = useFormContext();

    return (
        <Controller
            name={name}
            control={control}
            rules={{
                ...rules
            }}
            render={({
                field
            }) => (
                <FormControl fullWidth sx={{ m: 1 }} variant="outlined">
                    <TextField
                        {...field}
                        select
                        InputLabelProps={{shrink: true}}
                        id={name}
                        label={label}
                        size="small"
                        sx = {{mt: 2}}
                        error={!!errors[name]}
                        helperText={!!errors[name] ? errors[name]?.message as string : null}
                    >
                        {
                            menuItems.map((item) => {
                                return (
                                    <MenuItem value={item.value}>{item.key}</MenuItem>
                                )
                            })
                        }
                    </TextField>
                </FormControl>
            )} />
    )
}

export default FormInputSelect;
