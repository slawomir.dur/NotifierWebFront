import React from 'react'
import ReactDOM from 'react-dom/client'
import Root from './routes/root.tsx'
import { createBrowserRouter,
  RouterProvider } from 'react-router-dom'
import { routing } from './routes/routes'
import LoginPage from './components/LoginPage.tsx'
import AuthProvider from './auth/AuthProvider.tsx'
import Users from './routes/Users.tsx'

const router = createBrowserRouter([
  {
    path: "/",
    element: <LoginPage />
  },
  {
    path: "/app",
    element: <Root />,
    children: [
      {
        path: routing.users,
        element: <Users />
      },
      {
        path: routing.groups,
        element: null
      },
      {
        path: routing.notifications,
        element: null
      }
    ]
  }]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <AuthProvider>
      <RouterProvider router={router} />
    </AuthProvider>
  </React.StrictMode>,
)
