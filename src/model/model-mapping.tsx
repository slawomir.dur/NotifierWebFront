import * as React from 'react'
import PersonIcon from '@mui/icons-material/Person'
import GroupsIcon from '@mui/icons-material/Groups'
import CircleNotificationsIcon from "@mui/icons-material/CircleNotifications";
import { routing } from '../routes/routes'

type ModelMapping = {
    name: string,
    label: string,
    icon: React.ReactElement,
    route: string
};

export const userMapping: ModelMapping = {
    name: "users",
    label: "Użytkownicy",
    icon: <PersonIcon />,
    route: routing.users
};

export const groupsMapping: ModelMapping = {
    name: "groups",
    label: "Grupy",
    icon: <GroupsIcon />,
    route: routing.groups
};

export const notificationsMapping: ModelMapping = {
    name: "notifications",
    label: "Powiadomienia",
    icon: <CircleNotificationsIcon />,
    route: routing.notifications
};

export const modelMappings: ModelMapping[] = [userMapping, groupsMapping, notificationsMapping];
