export const AdminIndex = "1";
export const EditorIndex = "2";
export const ViewerIndex = "3";

export const Admin = "Admin";
export const Editor = "Editor";
export const Viewer = "Viewer";

export const toStringRole = (role: number) => {
    if (role.toString() === AdminIndex) {
        return Admin;
    } else if (role.toString() === EditorIndex) {
        return Editor;
    } else {
        return Viewer;
    }
}
