import { AuthResponse } from "../api/interfaces/authInterfaces"

const token: string = "token";


export const setToken = (auth: AuthResponse) => {
    sessionStorage.setItem(token, auth.token);
}

export const getToken = () => {
    return sessionStorage.getItem(token);
}