import * as React from 'react'
import { useAuth } from './AuthProvider'
import { Navigate } from 'react-router-dom'
import { routing } from '../routes/routes'

const Authenticated: React.FC<any> = ({ hide = false, children }: any) => {
    const { token } = useAuth()

    if (token === "") {
        if (hide) {
            return (
                <></>
            )

        }
        return (
            <Navigate to={routing.login} />
        )
    }

    return (
        <>
            {children}
        </>
    )
}

export default Authenticated;