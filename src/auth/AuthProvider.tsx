import React, { createContext, useContext, useState, useEffect } from 'react'
import { AuthResponse } from '../api/interfaces/authInterfaces';
import { toStringRole } from './roles';

const tokenKey = "token";
const roleKey = "role";

const AuthContext = createContext({
    token: "",
    login: (auth: any) => {},
    logout: () => {}
});

export const useAuth = () => {
    return useContext(AuthContext);
}

const AuthProvider: React.FC<any> = ({ children }) => {
    const [token, setToken] = useState(
        sessionStorage.getItem(tokenKey) === null ? "" : sessionStorage.getItem(tokenKey)
    );
    const [role, setRole] = useState(
        sessionStorage.getItem(roleKey) === null ? "" : sessionStorage.getItem(roleKey)
    );

    const login = (auth: AuthResponse) => {
        setToken(auth.token);

        const role = toStringRole(auth.role);

        setRole(role);
        sessionStorage.setItem(tokenKey, auth.token);
        sessionStorage.setItem(roleKey, role);
    }

    const logout = () => {
        setToken("");
        setRole("");
        sessionStorage.removeItem(tokenKey);
        sessionStorage.removeItem(roleKey);
    }

    return (
        <AuthContext.Provider value={{ token, role, login, logout }}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthProvider;