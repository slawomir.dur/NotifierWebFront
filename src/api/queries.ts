import { useQuery, useQueryClient, useMutation } from '@tanstack/react-query'
import { deleteUser, getUsers, postUser, putUser } from './httpUsers'
import { UserRequest } from './interfaces/userInterfaces'

export const useQueryAllUsers = () => {
   const query = useQuery({
    queryKey: ['users'],
    queryFn: () => getUsers().then((response) => response.data)
   })
   return query
}

export const useMutateUser = () => {
    const client = useQueryClient();
    const mutation = useMutation({
        mutationFn: (user: UserRequest) => putUser(user),
        onSuccess: () => {
            client.invalidateQueries({ queryKey: ['users']})
        }
    })
    return mutation;
}

export const useRemoveUser = () => {
    const client = useQueryClient();
    const mutation = useMutation({
        mutationFn: (uuid: string) => deleteUser(uuid),
        onSuccess: () => {
            client.invalidateQueries({ queryKey: ['users']})
        }
    })
    return mutation;
}

export const useAddUser = () => {
    const client = useQueryClient();
    const mutation = useMutation({
        mutationFn: (user: UserRequest) => postUser(user).then((response) => response.data),
        onSuccess: () => {
            client.invalidateQueries({ queryKey: ['users']})
        }
    })
    return mutation;
}
