import { AxiosResponse } from "axios";
import { httpClient } from "./httpClient";
import { GroupRequest, GroupResponse } from "./interfaces/groupInterfaces";

const urlPrefix: string = 'groups';

export const getGroups = () : Promise<AxiosResponse<GroupResponse[]>> => httpClient.get<GroupResponse[]>(`${urlPrefix}/`);
export const getGroup = (uuid: string) : Promise<AxiosResponse<GroupResponse>> => httpClient.get<GroupResponse>(`${urlPrefix}/${uuid}`)
export const postGroup = (body: GroupRequest) : Promise<AxiosResponse<GroupResponse>> => httpClient.post<GroupResponse>(`${urlPrefix}/`, body);
export const putGroup = (body: GroupRequest) : Promise<AxiosResponse<GroupResponse>> => httpClient.post<GroupResponse>(`${urlPrefix}/`, body);
export const deleteGroup = (uuid: string) : Promise<AxiosResponse<GroupResponse>> => httpClient.delete<GroupResponse>(`${urlPrefix}/${uuid}`);