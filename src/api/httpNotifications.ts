import { httpClient } from "./httpClient";
import { NotificationRequest, NotificationResponse } from "./interfaces/notificationInterfaces";

const urlPrefix = 'notifications';

export const getNotifications = () => httpClient.get<NotificationResponse[]>(`${urlPrefix}/`);
export const getNotification = (uuid: string) => httpClient.get<NotificationResponse>(`${urlPrefix}/${uuid}`)
export const postNotification = (body: NotificationRequest) => httpClient.post<NotificationResponse>(`${urlPrefix}/`, body);
export const putNotification = (body: NotificationRequest) => httpClient.post<NotificationResponse>(`${urlPrefix}/`, body);
export const deleteNotification = (uuid: string) => httpClient.delete<NotificationResponse>(`${urlPrefix}/${uuid}`);