import { httpClient } from "./httpClient";
import { UserRequest, UserResponse } from "./interfaces/userInterfaces";

const urlPrefix = 'users';

export const getUsers = () => httpClient.get<UserResponse[]>(`${urlPrefix}/`);
export const getUser = (uuid: string) => httpClient.get<UserResponse>(`${urlPrefix}/${uuid}`)
export const postUser = (body: UserRequest) => httpClient.post<UserResponse>(`${urlPrefix}/`, body);
export const putUser = (body: UserRequest) => httpClient.put<UserResponse>(`${urlPrefix}/${body.uuid}/`, body);
export const deleteUser = (uuid: string) => httpClient.delete<UserResponse>(`${urlPrefix}/${uuid}`);
