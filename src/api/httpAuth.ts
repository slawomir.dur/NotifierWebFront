import { httpClient } from "./httpClient";
import { AuthRequest, AuthResponse } from "./interfaces/authInterfaces";

const urlPrefix = 'auth_token';

export const postAuth = (body: AuthRequest) => httpClient.post_no_token<AuthResponse>(`${urlPrefix}/`, body);