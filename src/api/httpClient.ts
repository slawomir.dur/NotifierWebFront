import axios, { AxiosInstance, AxiosRequestConfig, InternalAxiosRequestConfig, AxiosResponse } from 'axios'
import { getToken } from '../auth/token';

const defaultConfig: AxiosRequestConfig = {
    baseURL: 'http://localhost:8000/',
    responseType: 'json',
    headers: {
        'Content-Type': 'application/json'
    }
};

const axiosInstance: AxiosInstance = axios.create(defaultConfig);
const axiosNoToken: AxiosInstance = axios.create(defaultConfig);

axiosInstance.interceptors.request.use(
    async (config: InternalAxiosRequestConfig) => {
        const token = getToken();
        if (token == null) {
            throw new Error("No token")
        }
        config.headers.Authorization = 'Token ' + token;
        config.baseURL += 'api/';
        return config;
    },
    (error: unknown) => {
        return Promise.reject(error);
    }
);

axiosNoToken.interceptors.request.use(
    async (config: InternalAxiosRequestConfig) => {
        config.baseURL += 'api/';
        return config;
    },
    (error: unknown) => {
        return Promise.reject(error);
    }
);

const _get = <T>(url: string, queryParams?: unknown): Promise<AxiosResponse<T>> => {
    return axiosInstance.get<T>(url, { params: queryParams });
};

const _post = <T>(url: string, body?: unknown, queryParams?: unknown): Promise<AxiosResponse<T>> => {
    return axiosInstance.post<T>(url, body, { params: queryParams });
};

const _post_no_token = <T>(url: string, body?: unknown, queryParams?: unknown): Promise<AxiosResponse<T>> => {
    return axiosNoToken.post<T>(url, body, { params: queryParams });
};

const _put = <T>(url: string, body?: unknown, queryParams?: unknown): Promise<AxiosResponse<T>> => {
    return axiosInstance.put<T>(url, body, { params: queryParams });
};

const _delete = <T>(url: string): Promise<AxiosResponse<T>> => {
    return axiosInstance.delete<T>(url);
};

export const httpClient = {
    get: _get,
    post_no_token: _post_no_token,
    post: _post,
    put: _put,
    delete: _delete
};
