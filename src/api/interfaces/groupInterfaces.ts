export interface GroupResponse {
    uuid: string
    name: string,
    description: string,
    creation_date: Date
};

export interface GroupRequest {
    uuid?: string,
    name: string,
    description: string
};