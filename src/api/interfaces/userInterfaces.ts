import { GroupResponse } from "./groupInterfaces"

export interface UserResponse {
    uuid: string,
    username: string,
    first_name: string,
    last_name: string,
    email: string,
    groups: GroupResponse[],
    creation_date: Date,
    modification_date: Date
};

export interface UserRequest {
    uuid?: string,
    username: string,
    first_name: string,
    last_name: string,
    email: string
}