import { GroupResponse, GroupRequest } from "./groupInterfaces"

export interface NotificationResponse {
    uuid: string,
    title: string,
    text: string,
    image?: string,
    url?: string,
    groups: GroupResponse[],
};

export interface NotificationRequest {
    uuid?: string,
    title: string,
    text: string,
    image?: string,
    url?: string,
    groups: GroupRequest[]
}