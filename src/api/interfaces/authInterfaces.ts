export interface AuthResponse {
    uuid: string,
    token: string,
    role: number
};

export interface AuthRequest {
    username: string,
    password: string
}