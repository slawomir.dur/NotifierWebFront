import * as React from 'react'
import Box from '@mui/material/Box'
import { Outlet } from 'react-router-dom'
import NotifierAppBar from '../components/NotifierAppBar'
import Toolbar from '@mui/material/Toolbar'
import MenuDrawer from '../components/MenuDrawer'
import Container from '@mui/material/Container'
import Authenticated from '../auth/Authenticated'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'

const Root: React.FC = () => {
    const queryClient = new QueryClient();

    return (
        <Authenticated>
            <QueryClientProvider client={queryClient}>
            <MenuDrawer />
            <Box component="main"
                sx={{
                    display: 'flex',
                    flex: '1 1 auto',
                    flexDirection: 'column',
                    width: '100%'
                }}>
                <NotifierAppBar />
                
                <Box>
                    <Toolbar />
                    <Container>
                        <Outlet />
                    </Container>
                </Box>
            </Box>
            </QueryClientProvider>
        </Authenticated>

    );
}

export default Root;