const loginRoute = "/";
const usersRoute = "users";
const groupsRoute = "groups";
const notificationsRoute = "notifications";

export const routing = {
    login: loginRoute,
    users: usersRoute,
    groups: groupsRoute,
    notifications: notificationsRoute
};

export const fromRoot = (route: string) => {
    return "/app/" + route;
}