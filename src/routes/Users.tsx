import * as React from 'react'
import {
    DataGrid, GridColDef,
    GridActionsCellItem, plPL, useGridApiRef, GridToolbarContainer, GridRowId
} from '@mui/x-data-grid';
import { Snackbar, Box, Button, Modal } from '@mui/material'
import Alert, { AlertProps } from '@mui/material/Alert'
import PersonAddAlt1Icon from '@mui/icons-material/PersonAddAlt1';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import GroupsIcon from '@mui/icons-material/Groups'
import UserForm from '../components/user-form/UserForm';
import ProgressCircle from '../components/ProgressCircle';
import MessageBar from '../components/MessageBar';
import { useMutateUser, useQueryAllUsers, useAddUser, useRemoveUser } from '../api/queries';

interface EditToolbarProps {
    setOpenModal: (open: boolean) => void;
}

const EditToolbar = (props: EditToolbarProps) => {
    const handleClick = () => {
        props.setOpenModal(true);
    }

    return (
        <GridToolbarContainer>
            <Button color="primary" startIcon={<PersonAddAlt1Icon />} onClick={handleClick}>
                Dodaj użytkownika
            </Button>
        </GridToolbarContainer>
    )
}

const Users: React.FC = () => {
    const gridApiRef = useGridApiRef();

    const allUsersQuery = useQueryAllUsers();
    const mutateUserQuery = useMutateUser();
    const addUserQuery = useAddUser();
    const removeUserQuery = useRemoveUser();

    const [snackbar, setSnackbar] = React.useState<Pick<AlertProps, 'children' | 'severity'> | null>(null);
    const [openModal, setOpenModal] = React.useState<boolean>(false);
    const [modifyUser, setModifyUser] = React.useState<GridRowId | null>(null);
    const handleCloseSnackbar = () => setSnackbar(null);

    const columns: GridColDef[] = [
        {
            field: 'username', headerName: "Nazwa użytkownika", minWidth: 100, flex: 1
        },
        {
            field: 'first_name', headerName: "Imię", minWidth: 60, flex: 1
        },
        {
            field: 'last_name', headerName: "Nazwisko", minWidth: 60, flex: 1
        },
        {
            field: 'role', headerName: "Rola", minWidth: 100, flex: 1
        },
        {
            field: 'email', headerName: "E-mail", minWidth: 100, flex: 1
        },
        {
            field: 'actions', type: 'actions', 'headerName': 'Czynności', width: 100,
            cellClassName: 'actions',
            getActions: (params) => {
                return [
                    <GridActionsCellItem
                        icon={<EditIcon />}
                        label="Edycja"
                        className="textPrimary"
                        onClick={() => {
                            setOpenModal(true)
                            setModifyUser(params.row)}
                        }
                        color="inherit"
                    />,
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => removeUserQuery.mutate(params.id)}
                        color="inherit"
                    />
                ]
            }
        }
    ]

    const title = modifyUser === null ? "Nowy użytkownik" : "Edytuj użytkownika"
    const buttonText = modifyUser === null ? "Dodaj użytkownika" : "Edytuj użytkownika"
    const action = modifyUser === null ? (user) => {
        setOpenModal(false);
        return addUserQuery.mutateAsync(user).then((response) => {
            setSnackbar({ children: 'Poprawnie dodano użytkownika', severity: 'success' })
            return response
        })
        .catch((reason) => {
            setSnackbar({ children: `Błąd dodawania użytkownika: ${reason}`, severity: 'error' })
        });
    } : (user) => {
        setOpenModal(false);
        setModifyUser(null);
        return mutateUserQuery.mutateAsync(user).then((response) => {
            setSnackbar({ children: 'Poprawnie zmodyfikowano użytkownika', severity: 'success' })
            return response
        })
        .catch((reason) => {
            setSnackbar({ children: `Błąd modyfikacji użytkownika: ${reason}`, severity: 'error'})
        })
    };

    if (allUsersQuery.isPending) {
        return (
            <ProgressCircle />
        )
    }

    if (allUsersQuery.isError) {
        return (
            <MessageBar info={`Błąd pobierania danych: ${allUsersQuery.error.name} - ${allUsersQuery.error.message}`} />
        )
    }

    return (
        <>
            {
                allUsersQuery.data !== undefined && (
                    <DataGrid
                        apiRef={gridApiRef}
                        localeText={plPL.components.MuiDataGrid.defaultProps.localeText}
                        sx={{ m: 1 }}
                        getRowId={(row) => row.uuid}
                        rows={allUsersQuery.data}
                        columns={columns}
                        initialState={{
                            pagination: {
                                paginationModel: { page: 0, pageSize: 15 },
                            },
                        }}
                        pageSizeOptions={[5, 10, 15, 20, 25, 30, 35]}
                        slots={{
                            toolbar: EditToolbar
                        }}
                        slotProps={{
                            toolbar: { setOpenModal }
                        }}
                    />
                )
            }
            {
                !!snackbar && (
                    <Snackbar
                        open
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                        onClose={handleCloseSnackbar}
                        autoHideDuration={6000}
                    >
                        <Alert {...snackbar} onClose={handleCloseSnackbar} />
                    </Snackbar>
                )
            }
            <Modal
                open={openModal}
                onClose={() => {
                    setModifyUser(null)
                    setOpenModal(false)}}
            >
                <Box sx={{
                    position: 'absolute' as 'absolute',
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                    bgcolor: 'background.paper',
                    border: '1px solid grey',
                    borderRadius: '4px',
                    boxShadow: 24,
                    p: 4
                }}>
                    <UserForm title={title} buttonText={buttonText} user={modifyUser} action={action}/>
                </Box>
            </Modal>
        </>
    )
}

export default Users;